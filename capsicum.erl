-module(capsicum).
-export([cap_enter/0, cap_getmode/0, cap_sandboxed/0, cap_fcntls_limit/2,
	 cap_ioctls_limit/2, cap_rights_limit/2]).
-on_load(init/0).

-include("capsicum.hrl").
-include_lib("eunit/include/eunit.hrl").

init() ->
	ok = erlang:load_nif("./capsicum_nif", 0).

cap_enter() ->
	{error, "nif_library_not_loaded"}.

cap_getmode() ->
	{error, "nif_library_not_loaded"}.

cap_sandboxed() ->
	{error, "nif_library_not_loaded"}.

cap_fcntls_limit(PORT, RIGHTS) ->
	{ok, FD} = prim_inet:getfd(PORT),
	cap_c_fcntls_limit(FD, RIGHTS).

cap_ioctls_limit(PORT, CMDS) ->
	{ok, FD} = prim_inet:getfd(PORT),
	cap_c_ioctls_limit(FD, CMDS).

cap_rights_limit(PORT, RIGHTS) ->
	{ok, FD} = prim_inet:getfd(PORT),
	cap_c_rights_limit(FD, RIGHTS).

cap_c_fcntls_limit(_FD, _RIGHTS) ->
	{error, "nif_library_not_loaded"}.

cap_c_ioctls_limit(_FD, _CMDS) ->
	{error, "nif_library_not_loaded"}.

cap_c_rights_limit(_FD, _RIGHTS) ->
	{error, "nif_library_not_loaded"}.

capsicum_test() ->
	[
	 ?assert(cap_sandboxed() =:= {ok, false}),
	 ?assert(cap_getmode() =:= {ok, 0})
	].
