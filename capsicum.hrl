% Keep in sync with /usr/include/sys/capsicum.h

-define(CAPRIGHT(IDX, BIT),	((1 bsl (57 + IDX)) bor BIT)).
-define(CAP_READ,		?CAPRIGHT(0, 16#1)).
-define(CAP_WRITE,		?CAPRIGHT(0, 16#2)).
-define(CAP_SEEK_TELL,		?CAPRIGHT(0, 16#4)).
-define(CAP_SEEK,		(?CAP_SEEK_TELL bor ?CAPRIGHT(0, 16#8))).
-define(CAP_PREAD,		(?CAP_READ bor ?CAP_WRITE)).
-define(CAP_PWRITE,		(?CAP_SEEK bor ?CAP_READ)).
-define(CAP_MMAP,		?CAPRIGHT(0, 16#10)).
-define(CAP_MMAP_R,		(?CAP_MMAP bor ?CAP_SEEK bor ?CAP_READ)).
-define(CAP_MMAP_W,		(?CAP_MMAP bor ?CAP_SEEK bor ?CAP_WRITE)).
-define(CAP_MMAP_X,		(?CAP_MMAP bor ?CAP_SEEK bor 16#20)).
-define(CAP_MMAP_RW,		(?CAP_MMAP_R bor ?CAP_MMAP_W)).
-define(CAP_MMAP_RX,		(?CAP_MMAP_R bor ?CAP_MMAP_X)).
-define(CAP_MMAP_WX,		(?CAP_MMAP_W bor ?CAP_MMAP_X)).
-define(CAP_MMAP_RWX,		(?CAP_MMAP_R bor ?CAP_MMAP_W bor ?CAP_MMAP_X)).
-define(CAP_CREATE,		?CAPRIGHT(0, 16#40)).
-define(CAP_FEXECVE,		?CAPRIGHT(0, 16#80)).
-define(CAP_FSYNC,		?CAPRIGHT(0, 16#100)).
-define(CAP_FTRUNCATE,		?CAPRIGHT(0, 16#200)).
-define(CAP_LOOKUP,		?CAPRIGHT(0, 16#400)).
-define(CAP_FCHDIR,		?CAPRIGHT(0, 16#800)).
-define(CAP_FCHFLAGS,		?CAPRIGHT(0, 16#1000)).
-define(CAP_CHFLAGSAT,		(?CAP_FCHFLAGS bor ?CAP_LOOKUP)).
-define(CAP_FCHMOD,		?CAPRIGHT(0, 16#2000)).
-define(CAP_FCHMODAT, 		(?CAP_FCHMOD bor ?CAP_LOOKUP)).
-define(CAP_FCHOWN,		?CAPRIGHT(0, 16#4000)).
-define(CAP_FCHOWNAT,		(?CAP_FCHOWN bor ?CAP_LOOKUP)).
-define(CAP_FCNTL,		?CAPRIGHT(0, 16#8000)).
-define(CAP_FLOCK,		?CAPRIGHT(0, 16#10000)).
-define(CAP_FPATHCONF,		?CAPRIGHT(0, 16#20000)).
-define(CAP_FSCK,		?CAPRIGHT(0, 16#40000)).
-define(CAP_FSTAT,		?CAPRIGHT(0, 16#80000)).
-define(CAP_FSTATAT,		(?CAP_FSTAT bor ?CAP_LOOKUP)).
-define(CAP_FSTATFS,		?CAPRIGHT(0, 16#100000)).
-define(CAP_FUTIMES,		?CAPRIGHT(0, 16#200000)).
-define(CAP_FUTIMESAT,		(?CAP_FUTIMES bor ?CAP_LOOKUP)).
-define(CAP_LINKAT_TARGET,	(?CAP_LOOKUP bor 16#400000)).
-define(CAP_MKDIRAT,		(?CAP_LOOKUP bor 16#800000)).
-define(CAP_MKFIFOAT,		(?CAP_LOOKUP bor 16#1000000)).
-define(CAP_MKNODAT,		(?CAP_LOOKUP bor 16#2000000)).
-define(CAP_RENAMEAT_SOURCE,	(?CAP_LOOKUP bor 16#4000000)).
-define(CAP_SYMLINKAT,		(?CAP_LOOKUP bor 16#8000000)).
-define(CAP_UNLINKAT,		(?CAP_LOOKUP bor 16#10000000)).
-define(CAP_ACCEPT,		?CAPRIGHT(0, 16#20000000)).
-define(CAP_BIND,		?CAPRIGHT(0, 16#40000000)).
-define(CAP_CONNECT,		?CAPRIGHT(0, 16#80000000)).
-define(CAP_GETPEERNAME,	?CAPRIGHT(0, 16#100000000)).
-define(CAP_GETSOCKNAME,	?CAPRIGHT(0, 16#200000000)).
-define(CAP_GETSOCKOPT,		?CAPRIGHT(0, 16#400000000)).
-define(CAP_LISTEN,		?CAPRIGHT(0, 16#800000000)).
-define(CAP_PEELOFF,		?CAPRIGHT(0, 16#1000000000)).
-define(CAP_RECV,		?CAP_READ).
-define(CAP_SEND,		?CAP_WRITE).
-define(CAP_SETSOCKOPT,		?CAPRIGHT(0, 16#2000000000)).
-define(CAP_SHUTDOWN,		?CAPRIGHT(0, 16#4000000000)).
-define(CAP_BINDAT,		(?CAP_LOOKUP bor 16#8000000000)).
-define(CAP_CONNECTAT,		(?CAP_LOOKUP bor 16#10000000000)).
-define(CAP_LINKAT_SOURCE,	(?CAP_LOOKUP bor 16#20000000000)).
-define(CAP_RENAMEAT_TARGET,	(?CAP_LOOKUP bor 16#40000000000)).
-define(CAP_SOCK_CLIENT,
	(?CAP_CONNECT bor ?CAP_GETPEERNAME bor ?CAP_GETSOCKNAME bor
	 ?CAP_GETSOCKOPT bor ?CAP_PEELOFF bor ?CAP_RECV bor ?CAP_SEND bor
	 ?CAP_SETSOCKOPT bor ?CAP_SHUTDOWN)).
-define(CAP_SOCK_SERVER,
	(?CAP_ACCEPT bor ?CAP_BIND bor ?CAP_GETPEERNAME bor ?CAP_GETSOCKNAME bor
	 ?CAP_GETSOCKOPT bor ?CAP_LISTEN bor ?CAP_PEELOFF bor ?CAP_RECV bor
	 ?CAP_SEND bor ?CAP_SETSOCKOPT bor ?CAP_SHUTDOWN)).
-define(CAP_ALL0,		?CAPRIGHT(0, 16#7FFFFFFFFFF)).
-define(CAP_UNUSED0_44,		?CAPRIGHT(0, 16#0000080000000000)).
-define(CAP_UNUSED0_57,		?CAPRIGHT(0, 16#0100000000000000)).
-define(CAP_MAC_GET,		?CAPRIGHT(1, 16#0000000000000001)).
-define(CAP_MAC_SET,		?CAPRIGHT(1, 16#0000000000000002)).
-define(CAP_SEM_GETVALUE,	?CAPRIGHT(1, 16#0000000000000004)).
-define(CAP_SEM_POST,		?CAPRIGHT(1, 16#0000000000000008)).
-define(CAP_SEM_WAIT,		?CAPRIGHT(1, 16#0000000000000010)).
-define(CAP_EVENT,		?CAPRIGHT(1, 16#0000000000000020)).
-define(CAP_KQUEUE_EVENT,	?CAPRIGHT(1, 16#0000000000000040)).
-define(CAP_IOCTL,		?CAPRIGHT(1, 16#0000000000000080)).
-define(CAP_TTYHOOK,		?CAPRIGHT(1, 16#0000000000000100)).
-define(CAP_PDGETPID,		?CAPRIGHT(1, 16#0000000000000200)).
-define(CAP_PDWAIT,		?CAPRIGHT(1, 16#0000000000000400)).
-define(CAP_PDKILL,		?CAPRIGHT(1, 16#0000000000000800)).
-define(CAP_EXTATTR_DELETE,	?CAPRIGHT(1, 16#0000000000001000)).
-define(CAP_EXTATTR_GET,	?CAPRIGHT(1, 16#0000000000002000)).
-define(CAP_EXTATTR_LIST,	?CAPRIGHT(1, 16#0000000000004000)).
-define(CAP_EXTATTR_SET,	?CAPRIGHT(1, 16#0000000000008000)).
-define(CAP_ACL_CHECK,		?CAPRIGHT(1, 16#0000000000010000)).
-define(CAP_ACL_DELETE,		?CAPRIGHT(1, 16#0000000000020000)).
-define(CAP_ACL_GET,		?CAPRIGHT(1, 16#0000000000040000)).
-define(CAP_ACL_SET,		?CAPRIGHT(1, 16#0000000000080000)).
-define(CAP_KQUEUE_CHANGE,	?CAPRIGHT(1, 16#0000000000100000)).
-define(CAP_KQUEUE,		(?CAP_KQUEUE_EVENT bor ?CAP_KQUEUE_CHANGE)).
-define(CAP_ALL1,		?CAPRIGHT(1, 16#00000000001FFFFF)).
-define(CAP_UNUSED1_22,		?CAPRIGHT(1, 16#0000000000200000)).
-define(CAP_UNUSED1_57,		?CAPRIGHT(1, 16#0100000000000000)).
-define(CAP_POLL_EVENT,		?CAP_EVENT).

% These constants may be predefined in some other Erlang module.
% TODO: Check it later.

-define(F_GETFL,		3).
-define(F_SETFL,		4).
-define(F_GETOWN,		5).
-define(F_SETOWN,		6).

-define(CAP_FCNTL_GETFL,	(1 bsl ?F_GETFL)
-define(CAP_FCNTL_SETFL,	(1 bsl ?F_SETFL)
-define(CAP_FCNTL_GETOWN,	(1 bsl ?F_GETOWN)
-define(CAP_FCNTL_SETOWN,	(1 bsl ?F_SETOWN)
-define(CAP_FCNTL_ALL,		(?CAP_FCNTL_GETFL bor ?CAP_FCNTL_SETFL bor
				 ?CAP_FCNTL_GETOWN bor ?CAP_FCNTL_SETOWN)).

