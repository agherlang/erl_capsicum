/*-
 * (c) Konrad Jopek, 2017.
 *
 * Direct port of capsicum functionalities into Erlang.
 */

#include <sys/types.h>
#include <sys/capsicum.h>

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "erl_nif.h"


static ERL_NIF_TERM
nif_return(ErlNifEnv *env, int ret, ERL_NIF_TERM retval)
{
	/*
	 * This function handles all system errors and converts them to tuple
	 * {error, "Error message"}.
	 *
	 * If call was successfull it only returns tuple:
	 * {ok, <<retval>>}.
	 */
	if (ret != 0) {
		return (enif_make_tuple2(env, enif_make_atom(env, "error"),
		    enif_make_string(env, strerror(errno), ERL_NIF_LATIN1)));
	}

	return (enif_make_tuple2(env, enif_make_atom(env, "ok"),
	    retval));
}

static ERL_NIF_TERM
nif_cap_enter(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	int ret;

	if (argc != 0)
		return (enif_make_badarg(env));

	ret = cap_enter();
	return (nif_return(env, ret, enif_make_string(env, "",
	    ERL_NIF_LATIN1)));
}

static ERL_NIF_TERM
nif_cap_getmode(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	u_int flags;
	int ret;

	if (argc != 0)
		return (enif_make_badarg(env));

	flags = 0;
	ret = cap_getmode(&flags);
	return (nif_return(env, ret, enif_make_uint(env, flags)));
}

static ERL_NIF_TERM
nif_cap_sandboxed(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{

	bool sandboxed;

	if (argc != 0)
		return (enif_make_badarg(env));

	sandboxed = cap_sandboxed();
	return (nif_return(env, 0, enif_make_atom(env,
	    sandboxed ? "true" : "false")));
}

static ERL_NIF_TERM
nif_cap_fcntls_limit(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	int fd, ret;
	uint32_t rights;

	if (argc != 2)
		return (enif_make_badarg(env));

	ret = enif_get_int(env, argv[0], &fd);
	if (ret == 0)
		return (enif_make_badarg(env));

	ret = enif_get_uint(env, argv[1], &rights);
	if (ret == 0)
		return (enif_make_badarg(env));

	ret = cap_fcntls_limit(fd, rights);
	return (nif_return(env, ret, enif_make_string(env, "",
	     ERL_NIF_LATIN1)));
}

static ERL_NIF_TERM
nif_cap_ioctls_limit(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	ERL_NIF_TERM val, tail;
	int i, fd, ret;
	unsigned long *cmds;
	unsigned int len;
	size_t ncmds;

	if (argc != 2)
		return (enif_make_badarg(env));

	ret = enif_get_int(env, argv[0], &fd);
	if (ret == 0)
		return (enif_make_badarg(env));

	/*
	 * Fetch list length and check for boundary conditions.
	 * Especially, empty list should be treated as error.
	 */
	if (enif_is_list(env, argv[1]) == 0)
		return (enif_make_badarg(env));

	ret = enif_get_list_length(env, argv[1], &len);
	if (ret == 0)
		return (enif_make_badarg(env));
	if (len == 0)
		return (enif_make_badarg(env));

	tail = argv[1];

	ncmds = len;
	cmds = enif_alloc(ncmds * sizeof(*cmds));
	/*
	 * XXX: should we really use assert here?
	 * Assert will effectively kill Erlang VM but it will be the case
	 * anyway if only beam runs out of memory.
	 */
	assert(cmds != NULL);

	for (i = 0; enif_get_list_cell(env, tail, &val, &tail); i++) {
		ret = enif_get_ulong(env, val, cmds + i);
		if (ret == 0) {
			enif_free(cmds);
			return (enif_make_badarg(env));
		}
	}

	ret = cap_ioctls_limit(fd, cmds, ncmds);
	enif_free(cmds);

	return (nif_return(env, ret, enif_make_string(env, "",
	    ERL_NIF_LATIN1)));
}

static ERL_NIF_TERM
nif_cap_rights_limit(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
	/*
	 * We do not want to support cap_rights_{init,set,get} as in case of
	 * Erlang everything can be stored in list.
	 */
	int i, fd, ret;
	unsigned int len;
	unsigned long right;
	cap_rights_t rights;
	ERL_NIF_TERM val, tail;

	if (argc != 2)
		return (enif_make_badarg(env));

	cap_rights_init(&rights);

	/* argv[0] is fd. */
	ret = enif_get_int(env, argv[0], &fd);
	if (ret == 0)
		return (enif_make_badarg(env));

	if (enif_is_list(env, argv[1]) == 0)
		return (enif_make_badarg(env));

	/* argv[1] is list of rights. */
	ret = enif_get_list_length(env, argv[1], &len);
	if (ret == 0)
		return (enif_make_badarg(env));
	/* List must not be empty. */
	if (len == 0)
		return (enif_make_badarg(env));

	tail = argv[1];

	for (i = 0; enif_get_list_cell(env, tail, &val, &tail); i++) {
		ret = enif_get_ulong(env, val, &right);
		if (ret == 0)
			return (enif_make_badarg(env));

		cap_rights_set(&rights, right);
	}

	/*
	 * Check if rights are valid before calling cap_rights_limit.
	 */
	if (!cap_rights_is_valid(&rights))
		return (enif_make_badarg(env));
	ret = cap_rights_limit(fd, &rights);
	return (nif_return(env, ret, enif_make_string(env, "",
	    ERL_NIF_LATIN1)));
}

static ErlNifFunc nif_funcs[] = {
	{"cap_enter",		0,	nif_cap_enter,		0},
	{"cap_getmode",		0,	nif_cap_getmode,	0},
	{"cap_sandboxed",	0,	nif_cap_sandboxed,	0},
	{"cap_c_fcntls_limit",	2,	nif_cap_fcntls_limit,	0},
	{"cap_c_ioctls_limit",	2,	nif_cap_ioctls_limit,	0},
	{"cap_c_rights_limit",	2,	nif_cap_rights_limit,	0}
};

ERL_NIF_INIT(capsicum, nif_funcs, NULL, NULL, NULL, NULL);
