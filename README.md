Capsicum support for Erlang VM
==============================

Introduction
------------

Erlang VM provides fair level of security, especially in terms of process
isolation. However, Erlang suffers from lack of well-done, sandboxing mechanism.
The intention for this project is to port Capsicum into Erlang.

Design goals
------------

The very first idea is to port all of the Capsicum-related functions from
FreeBSD's libc into Erlang using NIF.

Still, we will need to address some of the C vs. Erlang incompatibilieties, such
as: how to convert Erlang's list into variadic function call in C?

Target of the project
---------------------

The target is to deliver fully-functional Erlang port of Capsicum. The long-term
goal is use this library in RabbitMQ.

Compilation instructions
------------------------

In order to compile this extension, one will need FreeBSD installation
(preferred version is 11.0-RELEASE) and erlang installation. After successful
installation of base system, described here:

https://www.freebsd.org/doc/handbook/bsdinstall.html

you can bootstrap package system:

`pkg bootstrap`

and then install erlang and git:

`pkg install -y erlang git`

After that you will be able to compile the whole project:

`make erl_capsicum`
