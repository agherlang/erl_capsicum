SHLIB_NAME= capsicum_nif.so
SRCS= ./capsicum_nif.c

CFLAGS+= -I/usr/local/lib/erlang/usr/include -Wall -Werror

erl_capsicum: all
	erlc capsicum.erl

clean_all: clean
	rm -f capsicum.beam

.include <bsd.lib.mk>
