-module(testcaps).
-export([server/0, handle_connection/1]).

-include("capsicum.hrl").

server() ->
	{ok, ListenSock} = gen_tcp:listen(5678, [binary,
						 {active, false}]),
	capsicum:cap_enter(),
	capsicum:cap_rights_limit(ListenSock, [?CAP_ACCEPT, ?CAP_EVENT,
					       ?CAP_KQUEUE, ?CAP_KQUEUE_CHANGE,
					       ?CAP_KQUEUE_EVENT]),
	acceptor(ListenSock).

acceptor(ListenSock) ->
	{ok, ClientSock} = gen_tcp:accept(ListenSock),
	spawn(fun() -> handle_connection(ClientSock) end),
	acceptor(ListenSock).

handle_connection(Socket) ->
	capsicum:cap_rights_limit(Socket, [?CAP_RECV, ?CAP_SEND]),
	loop_connection(Socket).

loop_connection(Socket) ->
	case gen_tcp:recv(Socket, 0) of
		{ok, Data} ->
			case Data of
				<<"run\n">> -> io:fwrite("Hello!~n");
				_ -> gen_tcp:send(Socket, Data)
			end,
			loop_connection(Socket);
		{error, closed} ->
			gen_tcp:close(Socket)
	end.
