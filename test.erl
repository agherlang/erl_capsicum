-module(test).
-export([server/0, handle_connection/1]).

server() ->
	{ok, ListenSock} = gen_tcp:listen(5678, [binary,
						 {active, false}]),
	{ok, ListenFd} = prim_inet:getfd(ListenSock),
	io:fwrite("~w~n", [ListenFd]),
	acceptor(ListenSock).

acceptor(ListenSock) ->
	{ok, ClientSock} = gen_tcp:accept(ListenSock),
	spawn(fun() -> handle_connection(ClientSock) end),
	acceptor(ListenSock).

handle_connection(Socket) ->
	case gen_tcp:recv(Socket, 0) of
		{ok, Data} ->
			case Data of
				<<"run\n">> -> io:fwrite("Hello!~n");
				_ -> gen_tcp:send(Socket, Data)
			end,
			handle_connection(Socket);
		{error, closed} ->
			gen_tcp:close(Socket)
	end.
